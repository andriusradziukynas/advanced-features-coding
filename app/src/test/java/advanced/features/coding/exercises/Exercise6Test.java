package advanced.features.coding.exercises;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class Exercise6Test {

    @Test
    void formatFirstAndLast() {
        Map<String, String> tm = new TreeMap<>();
        tm.put("a", "a");
        tm.put("b", "b");
        tm.put("d", "d");

        assertEquals("Key: a, Value: a\nKey: d, Value: d\n", Exercise6.formatFirstAndLast(tm));
    }
}