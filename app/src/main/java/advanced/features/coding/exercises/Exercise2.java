package advanced.features.coding.exercises;

import java.util.Arrays;
import java.util.List;

public class Exercise2 {
    public static void main(String... args) {
        List<String> listToSort = Arrays.asList(
                "Jonas",
                "jaunas",
                "Benas",
                "burgeris",
                "Vitalijus",
                "Zebra"
        );

        new Exercise2().sortDescStringIgnoreCase(listToSort);
        System.out.println(listToSort);
    }

    /*
        Takes a string list as a parameter, then returns that list sorted alphabetically from Z to
        A case-insensitive.
    */
    private void sortDescStringIgnoreCase(final List<String> listToSort) {
        listToSort.sort((o1, o2) -> o2.toLowerCase().compareTo(o1.toLowerCase()));
        /*
            listToSort.sort(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o2.toLowerCase().compareTo(o1.toLowerCase());
                }
            });
        */
    }
}

