package advanced.features.coding.exercises;

import java.util.Arrays;

public class Exercise12 {
    public static void main(String[] args) {
        Manufacturer m1 = new Manufacturer("Fiat", 1999, "Italy");
        Manufacturer m2 = new Manufacturer("Fiat", 1998, "Italy");

        Car c1 = new Car(
                "Fiat",
                "Bravo",
                500.0,
                1998,
                Arrays.asList(m1, m2),
                Engine.V6
        );

        Car c2 = new Car(
                "Fiat",
                "Bravo",
                500.0,
                1998,
                Arrays.asList(m1, m2),
                Engine.V6
        );

        System.out.println(c1.equals(c2));
    }
}


