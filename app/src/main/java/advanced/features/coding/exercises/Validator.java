package advanced.features.coding.exercises;

public interface Validator {
    default boolean validate(Parcel input) {
        if (input.getX() + input.getY() + input.getZ() > 300) {
            System.out.println("Dimension (x, y, z) is > 300");
            return false;
        }

        if (input.getX() < 30 || input.getY() < 30 || input.getZ() < 30) {
            System.out.println("One of x, y or z is less than 30");
            return false;
        }

        if (input.isExpress() && input.getWeight() > 15.0F) {
            System.out.println("Weight for express is more than 15");
            return false;
        } else if (!input.isExpress() && input.getWeight() > 30.0F) {
            System.out.println("Weight for express is more than 30");
            return false;
        }

        return true;
    }
}
