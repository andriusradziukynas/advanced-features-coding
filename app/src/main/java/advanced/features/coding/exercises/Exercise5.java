package advanced.features.coding.exercises;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Exercise5 {
    public static void main(String... args) {
        SDAHashSet<String> s = new SDAHashSet<>();

        s.add("t");
        s.add("e");
        System.out.println(s.contains("t"));
    }

    public static class SDAHashSet<E> {
        private final transient HashMap<E,Object> map;
        private static final Object PRESENT = new Object();

        public SDAHashSet() {
            map = new HashMap<>();
        }

        public SDAHashSet(int initialCapacity) {
            map = new HashMap<>(initialCapacity);
        }

        public boolean add(E e) {
            return map.put(e, PRESENT) == null;
        }

        public boolean remove(E o) {
            return map.remove(o) == PRESENT;
        }

        public int size() {
            return map.size();
        }

        public boolean contains(E e) {
            return map.containsKey(e);
        }

        public void clear() {
            map.clear();
        }
    }
}
