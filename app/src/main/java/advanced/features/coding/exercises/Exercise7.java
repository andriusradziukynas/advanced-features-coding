package advanced.features.coding.exercises;

public class Exercise7 {
    public static void main(String... args) {
        Weapon w = new Weapon(5);

        w.loadBullet("1");
        w.loadBullet("2");
        w.loadBullet("3");
        w.loadBullet("4");
        w.loadBullet("5");
        w.loadBullet("6");

        System.out.println(w.isLoaded());
        System.out.println(w.shot());
        System.out.println(w.shot());
        System.out.println(w.shot());
        System.out.println(w.shot());
        System.out.println(w.shot());
        System.out.println(w.shot());
        System.out.println(w.shot());
    }
}
