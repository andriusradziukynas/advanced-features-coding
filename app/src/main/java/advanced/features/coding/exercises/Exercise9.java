package advanced.features.coding.exercises;

import java.util.Arrays;

public class Exercise9 {
    public static void main(String... args) {
        Circle c = new Circle(new Point2D(0,0), new Point2D(4,4));
        System.out.println(c.getRadius());
        System.out.println(c.getPerimeter());
        System.out.println(c.getRadius());
        System.out.println(Arrays.toString(c.getSlicePoints().toArray()));
    }
}
