package advanced.features.coding.exercises;

public class Exercise15 {
    public static void main(String[] args) {
        System.out.println(Car.FERRARI.isFasterThan(Car.FIAT));
        System.out.println(Car.FERRARI.isPremium());
    }

    public enum Car {
        FERRARI(320, false),
        PORSCHE(310, false),
        MERCEDES(220, false),
        BMW(210, false),
        OPEL(200, true),
        FIAT(190, true),
        TOYOTA(180, true);

        private final Integer speed;
        private final boolean isRegular;

        Car(Integer speed, boolean isRegular) {
            this.speed = speed;
            this.isRegular = isRegular;
        }

        boolean isPremium() {
            return !isRegular();
        }

        boolean isRegular() {
            return isRegular;
        }

        boolean isFasterThan(Car car) {
            return this.speed > car.speed;
        }
    }
}
