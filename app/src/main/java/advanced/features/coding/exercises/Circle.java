package advanced.features.coding.exercises;

import java.util.Arrays;
import java.util.List;

import static java.lang.Math.*;

public class Circle implements Move, Resizable {
    private Point2D center;
    private Point2D point;

    public Circle(Point2D center, Point2D point) {
        this.center = center;
        this.point = point;
    }

    public double getRadius() {
        double x2 = point.getX();
        double y2 = point.getY();
        double x1 = center.getX();
        double y1 = center.getY();
        return sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    }

    public double getPerimeter() {
        return getRadius() * 2 * Math.PI;
    }

    public double getArea() {
        return Math.PI * pow(getRadius(), 2);
    }

    List<Point2D> getSlicePoints() {
        double x0 = center.getX();
        double y0 = center.getY();
        Point2D p1 = new Point2D(x0 + getRadius() * cos(PI / 2), y0 + getRadius() * sin(PI / 2));
        Point2D p2 = new Point2D(x0 + getRadius() * cos(PI), y0 + getRadius() * sin(PI));
        Point2D p3 = new Point2D(x0 + getRadius() * cos(PI + PI / 2), y0 + getRadius() * sin(PI + PI / 2));
        return Arrays.asList(p1, p2, p3);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "center=" + center +
                ", point=" + point +
                '}';
    }

    @Override
    public void move(MoveDirection moveDirection) {
        double xd = moveDirection.getX();
        double yd = moveDirection.getY();

        this.center.setX(this.center.getX() + xd);
        this.center.setY(this.center.getY() + yd);

        this.point.setX(this.point.getX() + xd);
        this.point.setY(this.point.getY() + yd);
    }

    @Override
    public void resize(double resizeFactor) {
        double newX = (1 - resizeFactor) * center.getX() + resizeFactor * point.getX();
        double newY = (1 - resizeFactor) * center.getY() + resizeFactor * point.getY();

        point.setX(newX);
        point.setY(newY);
    }
}
