package advanced.features.coding.exercises;

public class Exercise8 {
    public static void main(String... args) {
        Parcel p = new Parcel(31,31, 31, 30, true);
        CommonValidator cv = new CommonValidator();
        cv.validate(p);
    }
}
