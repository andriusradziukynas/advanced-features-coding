package advanced.features.coding.exercises;

public interface Move {
    void move(MoveDirection moveDirection);
}
