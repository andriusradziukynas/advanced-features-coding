package advanced.features.coding.exercises;

public class Parcel {
    private int x;
    private int y;
    private int z;
    private float weight;
    private boolean isExpress;

    public Parcel(int x, int y, int z, float weight, boolean isExpress) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.weight = weight;
        this.isExpress = isExpress;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public boolean isExpress() {
        return isExpress;
    }

    public void setExpress(boolean express) {
        isExpress = express;
    }
}
