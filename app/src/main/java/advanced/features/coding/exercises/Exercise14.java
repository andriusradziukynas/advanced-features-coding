package advanced.features.coding.exercises;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Exercise14 {
    private static final int UPPER_BOUND = 100;
    private static final int ELEMENT_COUNT = 100;

    public static void main(String[] args) {
        Integer[] items = generateRandom();
        System.out.println(Arrays.toString(items));
        List<Integer> uniqueItems = getUniqueItems(items);
        uniqueItems.forEach(System.out::println);
        System.out.println("------------------------------");
        List<Integer> itemsWithDuplicates = getItemsWithDuplicates(items);
        itemsWithDuplicates.forEach(System.out::println);
        System.out.println("------------------------------");
        List<Integer> topElement = topDuplicateItems(items, 3);
        System.out.println(topElement);
        System.out.println("------------------------------");
        System.out.println(Arrays.toString(deduplicate(items)));
    }

    private static Integer[] generateRandom() {
        Random rand = new Random();
        Integer[] randomInt = new Integer[ELEMENT_COUNT];

        for (int i = 0; i < randomInt.length; i++) {
            randomInt[i] = rand.nextInt(UPPER_BOUND);
        }

        return randomInt;
    }

    public static List<Integer> getUniqueItems(Integer[] items) {
        return Arrays.stream(items)
                .filter(i -> Collections.frequency(Arrays.asList(items), i) == 1)
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<Integer> getItemsWithDuplicates(Integer[] items) {
        return Arrays.stream(items)
                .filter(i -> Collections.frequency(Arrays.asList(items), i) > 1)
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<Integer> topDuplicateItems(Integer[] items, int top) {
        return Arrays.stream(items)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 1)
                .sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue()))
                .limit(top)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public static Integer[] deduplicate(Integer[] items) {
        Random rand = new Random();
        List<Integer> itemsWithDuplicates = getItemsWithDuplicates(items);
        List<Integer> uniqueItems = getUniqueItems(items);

        for (int i = 0; i < items.length; i++) {
            if (itemsWithDuplicates.contains(items[i])) {
                while (true) {
                    Integer nextRandom = rand.nextInt(UPPER_BOUND);
                    if (!uniqueItems.contains(nextRandom)) {
                        items[i] = nextRandom;
                        break;
                    }
                }
            }
        }
        return items;
    }
}
