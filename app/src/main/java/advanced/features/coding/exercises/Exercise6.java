package advanced.features.coding.exercises;

import java.util.Iterator;
import java.util.Map;

public class Exercise6 {
    public static String formatFirstAndLast(Map<String, String> map) {
        Map.Entry<String, String> firstEntry;
        Map.Entry<String, String> lastEntry;
        Iterator<Map.Entry<String, String>> i = map.entrySet().iterator();
        if (i.hasNext()) {
            firstEntry = i.next();
            lastEntry = firstEntry;
            while (i.hasNext()) {
                lastEntry = i.next();
            }
            StringBuilder s = new StringBuilder();
            s.append("Key: ").append(firstEntry.getKey()).append(", Value: ").append(firstEntry.getValue()).append("\n");
            s.append("Key: ").append(lastEntry.getKey()).append(", Value: ").append(lastEntry.getValue()).append("\n");
            return s.toString();
        }
        return null;
    }
}

