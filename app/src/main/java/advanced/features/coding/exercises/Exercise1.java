package advanced.features.coding.exercises;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Exercise1 {
    public static void main(String... args) {
        List<String> listToSort = Arrays.asList(
                "Jonas",
                "Jaunas",
                "Benas",
                "Vitalijus",
                "Zebra"
        );

        new Exercise1().sortDescString(listToSort);

        System.out.println(listToSort);
    }

    /*
        Takes a string list as a parameter,
        then returns the list sorted alphabetically from Z to A.
    */
    private void sortDescString(final List<String> listToSort) {
        listToSort.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });

//        listToSort.sort(Comparator.reverseOrder());

//        listToSort.sort((o1, o2) -> o2.compareTo(o1));
    }
}
