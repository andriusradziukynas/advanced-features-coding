package advanced.features.coding.exercises;

public class Exercise10 {
    public static void main(String... args) {
        Circle c = new Circle(new Point2D(0,0), new Point2D(4,4));
        System.out.println(c.getPerimeter());
        System.out.println(c.getRadius());
        System.out.println(c);

        c.move(new MoveDirection(2, 2));

        System.out.println(c.getPerimeter());
        System.out.println(c.getRadius());
        System.out.println(c);
    }
}
