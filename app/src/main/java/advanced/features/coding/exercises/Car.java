package advanced.features.coding.exercises;

import java.util.List;
import java.util.Objects;

public class Car implements Comparable<Car> {
    private String name;
    private String model;
    private double price;
    private int year;
    private List<Manufacturer> manufacturers;
    private Engine engine;

    public Car(String name, String model, double price, int year, List<Manufacturer> manufacturers, Engine engine) {
        this.name = name;
        this.model = model;
        this.price = price;
        this.year = year;
        this.manufacturers = manufacturers;
        this.engine = engine;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<Manufacturer> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(List<Manufacturer> manufacturers) {
        this.manufacturers = manufacturers;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Double.compare(car.getPrice(), getPrice()) == 0 && getYear() == car.getYear() && Objects.equals(getName(), car.getName()) && Objects.equals(getModel(), car.getModel()) && Objects.equals(getManufacturers(), car.getManufacturers()) && getEngine() == car.getEngine();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getModel(), getPrice(), getYear(), getManufacturers(), getEngine());
    }


    @Override
    public int compareTo(Car o) {
        int compare = this.getName().compareTo(o.getName());

        if (compare == 0) {
            compare = this.getModel().compareTo(o.getModel());
        }

        return compare;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", year=" + year +
                ", manufacturers=" + manufacturers +
                ", engine=" + engine +
                '}';
    }
}
