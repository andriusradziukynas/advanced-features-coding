package advanced.features.coding.exercises;

public class Exercise11 {
    public static void main(String... args) {
        Circle c = new Circle(new Point2D(0, 0), new Point2D(4, 4));
        System.out.println(c.getRadius());
        System.out.println(c.getPerimeter());

        c.resize(2);
        System.out.println(c.getRadius());
        System.out.println(c.getPerimeter());
    }
}
